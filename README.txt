UC Teapplix

Integrates Ubercart 3.x (Drupal 7) with the Teapplix shipping and accounting automation service.

Features:

Imports Ubercart orders into your Teapplix account in real-time
Imports products, attributes & options, taxes, and shipping price for each order
Automatically updates order status in Ubercart when order is shipped in Teapplix (updated on cron)
Automatically adds tracking number from Teapplix to order comments in Ubercart (updated on cron)
Provides a basic report of shipped Teapplix orders accessible from within Drupal

Before use, ensure you set the required configuration options at admin/store/settings/uc_teapplix (requires administer store permission)

Testing has been limited to our client's Teapplix account, please contact us or post issues in the queue if you are using this module with a live Teapplix account.

Requires the php cURL library.

Developed by Problue Solutions

Sponsored by Buynesp.com